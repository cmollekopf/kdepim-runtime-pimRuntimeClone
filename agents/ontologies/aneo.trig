#
# Copyright (c) 2011 Christian Mollekopf <chrigi_1@fastmail.fm>
# Copyright (c) 2012 Sebastian Trueg <trueg@kde.org>
# All rights reserved, licensed under either CC-BY or BSD.
#
# You are free:
#  * to Share - to copy, distribute and transmit the work
#  * to Remix - to adapt the work
# Under the following conditions:
#  * Attribution - You must attribute the work in the manner specified by the author
#    or licensor (but not in any way that suggests that they endorse you or your use 
#    of the work).
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#  * Redistributions of source code must retain the above copyright notice, this 
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice, this 
#    list of conditions and the following disclaimer in the documentation and/or 
#    other materials provided with the distribution.
#  * Neither the names of the authors nor the names of contributors may
#    be used to endorse or promote products derived from this ontology without 
#    specific prior written permission.
#
# THIS ONTOLOGY IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS ONTOLOGY, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix nao:     <http://www.semanticdesktop.org/ontologies/2007/08/15/nao#> .
@prefix nrl:     <http://www.semanticdesktop.org/ontologies/2007/08/15/nrl#> .
@prefix nie:     <http://www.semanticdesktop.org/ontologies/2007/01/19/nie#> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix nfo:     <http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#> .

@prefix aneo:    <http://akonadi-project.org/ontologies/aneo#> .

aneo: {

    aneo:AkonadiDataObject a rdfs:Class ;
        rdfs:subClassOf nie:DataObject ;
        rdfs:label "AkonadiDataObject" ;
        rdfs:comment "used to identify akonadi entities (items and collections) created by the akonadi-nepomuk feeders" ;
        nao:userVisible false .

    aneo:akonadiItemId a rdf:Property ;
        rdfs:label "Akonadi Item ID" ;
        rdfs:comment "used to identify items created by the akonadi-nepomuk feeders (depreceated usage, use the AkonadiDataObject type instead)" ;
        rdfs:range xsd:string ;
        rdfs:domain aneo:AkonadiDataObject ;
        rdfs:subPropertyOf nao:identifier ;
        nrl:maxCardinality 1 ;
        nao:userVisible false .

    aneo:akonadiIndexCompatLevel a rdf:Property ;
        rdfs:label "Akonadi index compat level" ;
        rdfs:comment "Saves the version of the indexer used to write this item." ;
        rdfs:range xsd:int ;
        rdfs:domain aneo:AkonadiDataObject ;
        nrl:cardinality 1 ;
        nao:userVisible false .
}


<http://akonadi-project.org/ontologies/aneo/metadata#> {

    aneo: a nrl:Ontology ;
        nao:prefLabel "Akonadi Ontology" ;
        nao:hasDefaultNamespace "http://akonadi-project.org/ontologies/aneo#" ;
        nao:hasDefaultNamespaceAbbreviation "aneo" ;
        nao:lastModified "2012-02-15T09:56:43Z" ;
        nao:serializationLanguage "TriG" ;
        nao:status "Unstable" ;
        nrl:updatable "0" ;
        nao:version "2" .

    <http://akonadi-project.org/ontologies/aneo/metadata#>
        a nrl:GraphMetadata ;
        nrl:coreGraphMetadataFor aneo: .
}

